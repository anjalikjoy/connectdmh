<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>about</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <?php include 'includes/links.php';?> 
     </head>
    <body>
    <header class='header_menu'>
        <?php include 'includes/header.php';?>
    </header>
    <div class="about">
	    <div class="container">
	       	<div class="row">
	       		<div class="col-xs-12 col-sm-12 colmd-12 col-lg-12">
                    <h2>About Us</h2>
                    <p>WE, ConnectDmh Care is the No. 1 Geriatric Care Supplier and Support in Asia.  Our company has consulting offices covering most region supported by our collaboration partner’s training centres, to guide and empower the nurses and care-givers to a specific skill to cater for our fast-rapid ageing population countries.</p>
                    <p>We focused on providing the best nurses and care-givers to hospitals, healthcare homes or even home-based to families needing the help for their aged family members.</p>
                    <p>We recognize that a good caregiver takes away the stress and worries from their families’ members. Together, with our online services which can assists via our modern IT technology and apps, this continuous support provide our geriatric and family members the best support through their finest years.</p>
                    <p>ConnectDmh Care provides you with a wide choice of trained helpers and geriatric caregivers specially selected from Philippines, Indonesia, Cambodia, Myanmar and India. They are all trained with different skill sets based on some of our collaboration partners who specially tailored and design the training programmes strictly accordance to the requirements of each individual countries.</p>
                    <p>For skill sets needed for the assigned nurses or caregiver, ConnectDmh Care can provide, during the contract period, follow up programs and advisories from its base of geriatric doctors, nurses and physiotherapist. </p>
                    <p>ConnectDmh Care, stays as the best pivotal point to all our grey foxes and achieve their needs and support throughout their finest years. We, cares.</p>
                </div>
	       		
	       	</div>
	   	</div>
    </div>
	<footer class='footer_section'>
        <?php include 'includes/footer.php';?>
    </footer>

    </body>
</html>
