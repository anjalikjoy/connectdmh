
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<a href="#"><img class="img-responsive" src="images/logo.png"></a>
			</div>
			
			<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 hidden-sm hidden-md hidden-lg">

				<nav class="navbar navbar-default">
					<div class="navbar-header">
			            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
			                <span class="sr-only">Toggle navigation</span>
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>
			            </button>
			     
			        </div>
        
         			<div id="navbarCollapse" class="collapse navbar-collapse">
            			<ul class="nav navbar-nav">
				  			<li><a href="#">HOME</a></li>
					      	<li><a href="about.php">ABOUT US</a></li>
					      	<li><a href="#">SERVICES</a></li>
					      	<li><a href="#">ARTICLE</a></li>
					      	<li><a href="#">CONTACT</a></li>
					      	<li><a href="#">SEARCH</a></li>
					      	<li><a href="#" class="btn btn-info hidden-sm hidden-md hidden-lg">
				        		<span class="glyphicon glyphicon-shopping-cart "></span> Shopping Cart
				    		</a></li>
					      	<li><a href="#" class="btn btn-info hidden-sm hidden-md hidden-lg">Sign Up</a></li>
				    	</ul>

				  	</div>
				</nav>
	
			</div>
			<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10"></div>
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<div class="btn-group hidden-xs">
				  <a href ='#' class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				   <img src="images/icon.png">
				  </a>
				  <div class="dropdown-menu">
				    <a class="dropdown-item" href="index.php">HOME</a>
				    <div class="dropdown-divider"></div>
				    <a class="dropdown-item" href="about.php">ABOUT US</a>
				    <div class="dropdown-divider"></div>
				    <a class="dropdown-item" href="#">SERVICES</a>
				    <div class="dropdown-divider"></div>
				    <a class="dropdown-item" href="#">CONTACT</a>
				    <div class="dropdown-divider"></div>
				    <a class="dropdown-item" href="#">SEARCH</a>
				    <div class="dropdown-divider"></div>
				    <a href="#" class="btn btn-default">
				        <span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart
				    </a>
				  </div>
				</div>
				<a href="#" class="btn btn-info hidden-xs">Sign Up</a>
			</div>	
    		
		</div>
	</div>
			
