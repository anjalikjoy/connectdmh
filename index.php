<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>connectdmh</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <?php include 'includes/links.php';?> 
     </head>
    <body>
    <header class='header_menu'>
        <?php include 'includes/header.php';?>
    </header>
    <div class="container">
	    <div class="row">
	       	<div class="col-xs-12 col-sm-12 colmd-12 col-lg-12">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="images/1.jpg" alt="image1" class="img-responsive">
                        </div>
                        <div class="item">
                            <img src="images/2.jpg" alt="image2" class="img-responsive">
                        </div>
                        <div class="item">
                             <img src="images/3.jpg" alt="image3" class="img-responsive">
                        </div>
                        <div class="item">
                            <img src="images/4.jpg" alt="image4" class="img-responsive">
                        </div>
                        <div class="item">
                             <img src="images/5.jpg" alt="image5" class="img-responsive">
                        </div>
                        <div class="item">
                            <img src="images/6.jpg" alt="image6" class="img-responsive">
                        </div>
                    </div>
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class='about_main'>
        <div class='container'>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    
                    <p>We, ConnectDmh Care is the No. 1 Geriatric Care Supplier and Support in Asia. We focused on providing the best nurses and care-givers support to hospitals , healthcare homes or even home-based to families looking for help for their senior family members. </p>
                </div>
            </div>
        </div>
    </div>

    <div class='container'>
        <div class='row'>
        
             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="ser_img">
                    <img src="images/logo.png" class="img-responsive">
                </div>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h2 align="center">Services</h2>
            </div>
            
            <div class="services">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="ser1">
                      <div class="small">
                        <div class="text short">
                            <h3>Online courses</h3>
                            <p>CDMH care provide further training through our online courses,we believe in regularly empowering our care givers with new courses, will enhance and improve their knowledge</p>
                        
                                <h3>Online courses for nurse and caregiver</h3>
                                <p>Stroke Brain Interaction activity exercise</p>
                                <ul>
                                    <li>Module One</li>
                                    <li>Module Two</li>
                                    <li>Module Three</li>
                                </ul>
                                <p>Daily physio massages</p>
                                <ul>
                                    <li>Module One(Only suitable for independent fail and semi dependent)</li>
                                    <li></li>
                                    <li></li>
                                    <li>Dementia</li>
                                    <li>Packinson</li>
                                    <li>Physio exercise</li>
                                    <li>Hdyro therapy</li>
                                </ul>
                           
                        </div> 
                         
                        
                        <button class='btn btn-warning btn-sm read-more' >Read More</button>
                        </div>
                    </div>
                </div>
              <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="ser1">
                        <div class="small">
                        <div class="text short">
                            <h3>Products online</h3>
                            <p>CDmh Care online products provides all our grey foxes from health to wellness care. We believe our grey foxes should have the joy of keeping aging in place.</p>
                                <br/> <br/>
                                    <ul>
                                        <li>Medical Care</li>
                                        <li>Hospital Care</li>
                                        <li>Wellness Care </li>
                                    </ul>
                         </div> 
                        <button class='btn btn-warning btn-sm read-more' >Read More</button>
                        </div>
                    </div>
                </div>
                 <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="ser1">
                        <div class="small">
                        <div class="text short">
                        <h3>Shines After Life</h3>
                        <p>Nirvana Shines in all our life. We embrace and accept with the life we have all live and enjoy the fullest with our loving family and friends. Embrace the light, CDmh Care will assists your family to wonderful memories for all our grey foxes’ eternity. We Live On.</p>
                        
                                <ul>
                                    <li>Nirvana After Care Services and Packages</li>
                                    <li>Nirvana Photo Taking</li>
                                    <li>Nirvana Video Memories</li>
                                    <li>Nirvana Ceremonial Pre-Packages</li>
                                    <li>Nirvana Insurance</li>
                                    <li class="dropdown">
                                        <a data-toggle="dropdown">Nirvana 24 hour service support
                                        <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                           <li> <a class="btn btn-danger"  href="#">Sign In</a></li>
                                        </ul>
                                    </li>
                                </ul>
                           
                        </div>    
                       
                            <button class='btn btn-warning btn-sm read-more' >Read More</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <img src="images/img1.jpg" alt="image1" class="img-responsive">
                   <!--  <img src="images/img2.jpg" alt="image2" class="img-responsive"> -->
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <img src="images/img3.jpg" alt="image2" class="img-responsive">
                    <!-- <img src="images/img4.jpg" alt="image2" class="img-responsive"> -->
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class='container'>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <h3>What is Lorem Ipsum?</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <h3>Where does it come from?</h3>
                        <p>Where does it come from?Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
                </div>
            </div>
        </div>
    </div>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d16327779.219062492!2d108.82675093480469!3d-2.415160625084302!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2c4c07d7496404b7%3A0xe37b4de71badf485!2sIndonesia!5e0!3m2!1sen!2sin!4v1521638175710" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>

	<footer class='footer_section'>
        <?php include 'includes/footer.php';?>
    </footer>

    </body>
</html>
<script>
$(document).ready(function(){    
    $(".read-more").click(function(){        
        var $elem = $(this).parent().find(".text");
        if($elem.hasClass("short"))
        {
            $elem.removeClass("short").addClass("full");        
        }
        else
        {
            $elem.removeClass("full").addClass("short");        
        }       
    });
});

</script>